
# Printing the url we can access to our service
output "supermario_loadbalancer_url" {
  value = aws_elb.supermario.dns_name
}

output "supermario_instances_public_ips" {
  value = data.aws_instances.active_instances.public_ips
}

### The Ansible inventory file
resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tmpl",
    {
      public-ips = data.aws_instances.active_instances.public_ips
    }
  )
  filename = "inventory"
}
