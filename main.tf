provider "aws" {}

# Defining default subnet 1
resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

# Defining default subnet 2
resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

# Creating key-pair for SSH to remote access by Ansible
resource "aws_key_pair" "supermario_key_pair" {
  key_name   = "supermario-key-pair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAlsB0U7rdMTuanuT9FnWWTm1vlXunmwArK2RTz04/qGR/NyTD7q7F1kfTkmBHHV5/trZRaWOOLsix2sXll6tSdBjf7PNyhEZ+A3l+xUfkepryOYdVhwyQY/+IgcfgP34+1Xg43mgsxg6eU/7Wr7+Mr6PF3ePUuMlxD1HDbpTcufY5wN1njjktY9WaXbi0TGHz5m48eneeGUtafjg+eJ+JFmrrtk1mvt5jMUvMsJECbGwqxvjncT74bGy6hUzLUW89Mh735PyMycDx80hs579A9iuutOf/D4IUWiqBdK20+antAigVrkwxGq4olcMcFgjSQdAQ3kLLD2LBeNBJdRyy7Q== rsa-key-20210724"
}

# Creating new VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  #  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}

# Creating internet gateway for VPC
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "main-internet-gateway"
  }
}

# Creating route table for internet gateway
resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

# Creating a subnet for instances
resource "aws_subnet" "subnet_1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.10.0/24"

  availability_zone       = data.aws_availability_zones.available.names[0]
  map_public_ip_on_launch = "true"

  tags = {
    Name = "SuperMario-Subnet-1"
  }
}

# Creating Route table assotiation to set subnet public
resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.subnet_1.id
  route_table_id = aws_route_table.rtb_public.id
}

# Defining firewall rules
resource "aws_security_group" "supermario_sg" {
  name   = "SuperMario security group"
  vpc_id = aws_vpc.main.id

  # For HTTP
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # For SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "SuperMario security group"
    Owner = "Erik Arustamyan"
  }
}

# Defining the base configuration for our instances
resource "aws_launch_configuration" "supermario" {
  name_prefix     = "SuperMario-Ubuntu-Server-"
  image_id        = data.aws_ami.latest_ubuntu.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.supermario_sg.id]
  key_name        = aws_key_pair.supermario_key_pair.key_name
  user_data       = file("instance_init.sh")

  lifecycle {
    create_before_destroy = true
  }
}

# Creating auto-scaling group to controll service availability automaticly
resource "aws_autoscaling_group" "main" {
  name_prefix          = "ASG-${aws_launch_configuration.supermario.name}"
  launch_configuration = aws_launch_configuration.supermario.name
  min_size             = 2
  max_size             = 2
  min_elb_capacity     = 2
  health_check_type    = "ELB"
  vpc_zone_identifier  = [aws_subnet.subnet_1.id]
  load_balancers       = [aws_elb.supermario.name]

  dynamic "tag" {
    for_each = {
      Name  = "SuperMario-Server"
      Owner = "Erik Arustamyan"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Creating a load-balancer for our instances
resource "aws_elb" "supermario" {
  name = "SuperMario-ELB"
  #availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  subnets         = [aws_subnet.subnet_1.id]
  security_groups = [aws_security_group.supermario_sg.id]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 10
    target              = "TCP:22"
    interval            = 15
  }

  tags = {
    Name = "SuperMario ELB"
  }
}

# Executing ansible playbook
resource "null_resource" "run-ansible-playbook" {
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory docker-image-install.yml"
  }

  # Run after aws autoscaling group is ready
  depends_on = ["aws_autoscaling_group.main"]
}
