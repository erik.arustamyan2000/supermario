# Getting all availability zones
data "aws_availability_zones" "available" {}

# Getting the latest version of Ubuntu AMI
data "aws_ami" "latest_ubuntu" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

# Getting active aws instances by aws autoscaling group id
data "aws_instances" "active_instances" {
  instance_tags = {
    "aws:autoscaling:groupName" = "${aws_autoscaling_group.main.name}"
  }

  instance_state_names = ["running"]
}
