# SuperMario - WEB
Created for TalkDesk

## Installation
 Installation process has 4 steps.

Installing IaC tool
https://learn.hashicorp.com/tutorials/terraform/install-cli

Installing configuration management tool:
https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

For connecting to AWS export ENV variables
 ```
export AWS_ACCESS_KEY_ID=?
export AWS_PRIVATE_ACCESS_KEY=?
export AWS_DEFAULT_REGION=?
 ```
For initializing terraform missing files
 ```bash
 terraform init
 ```

 ## Running
 To run you just need to run
 ```
 sudo terraform apply
 ```
